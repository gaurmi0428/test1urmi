﻿//-Urmi Jadeja Test-1 Hands On DATE:25 FEB 2022 7:25 PM 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test1Urmi
{
    //Samrtphone class inherited from Phone class
    public class SmartPhone : Phone
    {
        //feilds
        private int storage;
        private int cameras;
        private bool isconnected;

        //Properties
        public int Storage
        {
            get => storage;
            set
            {
                if (value > 1 && value < 2048)
                    storage = value;
            }
        }
        public int Cameras
        {
            get => cameras;
            set
            {
                if (value >= 0 && value <= 5)
                    cameras = value;
            }
        }
        public bool Isconnected
        {
            get => isconnected;
            set => isconnected = value;
        } 

        //readonly properties: override
        public override string GetPhoneInfo
        {
            get
            {
                string info;
                info = this.storage.ToString() + "GB " + this.cameras.ToString() + " Cameras";
                return base.GetPhoneInfo + info; 
            }
        }

        //indexer
        public string this[int i] => FindIndex(i);  
        private string FindIndex(int i)
        {
            string msg;
           
            if (i < 0)
            {
                throw new Exception("Do not allow negative number");
            }
            else if ((i % 2 == 0) || (i == 0))
            {
                msg = this.storage + "GB";
            }
            else
            {
                msg = this.cameras + " Cameras";
            }
            return msg;
        }
        
        //constructor
        public SmartPhone( string modelName, Dimension phoneDescription, Voltage phoneUsage, int phoneStorage, int phoneCamera, bool isConnected) : base(modelName,phoneDescription, phoneUsage)
        {
            this.storage = phoneStorage;
            this.cameras = phoneCamera;
            this.isconnected = isConnected;
        }

        //methods
        public override string ToString()
        {
            string ans;
            if (this.Isconnected == true)
                ans = "Connected..";
            else
                ans = "Disconnected...";
            return $"{base.ToString()} ,{ans}";
        }              
    }
}
