﻿//-Urmi Jadeja Test-1 Hands On DATE:25 FEB 2022 7:25 PM 
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Test1Urmi
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            // string for test
            string answer =" ";
            
            //Dimension structure test
            Dimension dim = new Dimension(10.20, 5.30, 1.50);
            answer += "\n\n Dimesnsion ToString():";
            answer += $"\n Dimension:{dim.ToString()} ";

            //Phone class test
            Phone mobile = new Phone("Samsung", dim, Voltage.DUAL);
            answer += "\n\n Phone ToString():";
            answer += $"\n {mobile.ToString()} ";
            answer += "\n\n Phone Getinfo:";
            answer += $"\n Info:{ mobile.GetPhoneInfo} ";

            //Smartphone class test
            Dimension dime = new Dimension(12.10, 4.50, 2.1);
            SmartPhone smart = new SmartPhone("Nokia",dime,Voltage.V110,256,2,true);
            string index = smart[ 3 ];
            answer += "\n\n SmartPhone ToString():";
            answer += $"\n {smart.ToString()} ";
            answer += "\n\n SmartPhone Getinfo:";
            answer += $"\n Info:{ smart.GetPhoneInfo} ";
            answer += "\n\n SmartPhone Indexer:";
            answer += $"\n {index} ";

            //final output diplay on textblock
            txtDisplay.Text = answer;
        }
    }
}
