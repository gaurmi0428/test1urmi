﻿//-Urmi Jadeja Test-1 Hands On DATE:25 FEB 2022 7:25 PM 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test1Urmi
{
    //Structure for dimension
    public struct Dimension
    {
        //fields 
        private double height;
        private double width;
        private double depth;

        //properties
        public double Height
        {
            get => height;
            set
            {
                if (value > 0.0)
                    height = value;
            }

        }
        public double Width
        {
            get => width;
            set
            {
                if (value > 0.0)
                    width = value;
            }

        }
        public double Depth
        {
            get => depth;
            set
            {
                if (value > 0.0)
                    depth = value;
            }

        }

        //constructor
        public Dimension(double phoneHeight, double phoneWidth, double phoneDepth)
        {
            this.height = phoneHeight;
            this.width = phoneWidth;
            this.depth = phoneDepth;
        }

        //methods
        public override string ToString()
        {
            return $"({Height} x {Width} x {Depth})";
        }
    }
}
