﻿//-Urmi Jadeja Test-1 Hands On DATE:25 FEB 2022 7:25 PM 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test1Urmi
{
    // Phone Class
    public class Phone:Object
    {
        //fields
        private string modelname;
        private Dimension description = new Dimension();
        private Voltage voltage = Voltage.DUAL;

        //Properties
        public string Modelname
        {
            get => this.modelname;
            set => this.modelname = value;
        }

        //read only properties
        public virtual  string GetPhoneInfo
        {
            get
            {
                string display;
                if (this.voltage == Voltage.V110)
                    display =  "V110 US Only";
                else if (this.voltage == Voltage.V220)
                    display =  "V220 Eurpoe Only";
                else
                    display =  " Dual Voltage";
                return $" {this.modelname}  {description.ToString()}  {display} " ;
            }
        }
        
        //constructor
        public Phone(string modelName, Dimension phoneDescription, Voltage phoneUsage):base()
        {
            this.voltage = phoneUsage;
            this.description = phoneDescription;
            this.modelname = modelName;
        }
        
        //methods
        public override string ToString()
        {
            return $"{Modelname}";
        }

    }
}
